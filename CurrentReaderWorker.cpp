#include "CurrentReaderWorker.h"

CurrentReaderWorker::CurrentReaderWorker(const QString &thingSpeakAPIWriteKey, QObject *parent ) :
    QObject( parent )
  , m_httpRequest( )
  , m_timer( new QTimer( this ) )
  , m_getReqTimer( new QTimer( this ) )
  , m_lastTs( 0 )
  , m_currentTs( QDateTime::currentMSecsSinceEpoch() )
  , m_voltage( 0.0f )
  , m_current( 0.0f )
  , m_rmsCalc( new RMSCalc( 1000 ) )
  , THINGSPEAK_WRITEAPI_KEY( thingSpeakAPIWriteKey )
  , THINGSPEAK_BASE_URL( "https://api.thingspeak.com/update?api_key=%1&field1=%2&field2=%3" )
{
    connect( &m_httpRequest, &QNetworkAccessManager::finished, this, &CurrentReaderWorker::httpRequestReply );
    connect( m_timer, &QTimer::timeout, this, &CurrentReaderWorker::doWork );
    connect( m_getReqTimer, &QTimer::timeout, this, &CurrentReaderWorker::doHttpGETRequest, Qt::DirectConnection );

    m_getReqTimer->start( HTTP_UPDATE_TIMETOUT );

    /*--- setup the spi in the raspberry PI ---*/
    wiringPiSetup();
    mcp3004Setup( BASE, SPI_CHAN );

    /*--- Setup the timers. ---*/
    m_timer->start( RMS_CALC_TIMEOUT );
}

void CurrentReaderWorker::doWork()
{
    m_currentTs = QDateTime::currentMSecsSinceEpoch();
    m_voltage = analogRead( BASE+0 )* AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS );
    m_current = m_rmsCalc->calculateRMS( ( m_voltage - 2.5f) , (m_currentTs - m_lastTs)/1000.0f );
    m_lastTs = m_currentTs;

    qDebug() << m_current;
}

void CurrentReaderWorker::httpRequestReply(QNetworkReply *reply)
{
    qDebug() << "Networkn Replied: " << reply->error();
    reply->deleteLater();
}

void CurrentReaderWorker::doHttpGETRequest()
{
    QString newString( THINGSPEAK_BASE_URL.arg( THINGSPEAK_WRITEAPI_KEY ).arg( m_current ).arg( QDateTime::currentMSecsSinceEpoch() ) );
    QUrl url( newString );
    m_httpRequest.get( QNetworkRequest( url ) );
}

