QT += core network
QT -= gui

TARGET = RPIHelloWorld
CONFIG += console
CONFIG -= app_bundle

CONFIG += c++11

target.files = RPIHelloWorld
target.path = /home/alarm/

INSTALLS += target

TEMPLATE = app

INCLUDEPATH += /home/bmendonca/dev_tools/rpi-tools/rasp-pi-rootfs/root/usr/local/include

LIBS += -lwiringPi

SOURCES += main.cpp \
    RMSCalc.cpp \
    CurrentReaderWorker.cpp

HEADERS += \
    RMSCalc.h \
    CurrentReaderWorker.h

